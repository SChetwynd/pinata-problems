import process from 'node:process';
import 'dotenv/config';

import pinataSDK from '@pinata/sdk';

const pinataJWT = process.env.PINATA_JWT;

const pinata = new pinataSDK({ pinataJWTKey: pinataJWT });

try {
    const result = await pinata.pinFromFS('./to_upload');

    console.log(result);
} catch (err) {
    console.log(err.message);
}
